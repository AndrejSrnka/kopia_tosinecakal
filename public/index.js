
function playAudio() {
    var audio = document.getElementById("audio");
    var mutedIcon = document.getElementById("muted");
    if (! audioPlaying){
        audio.play();
        mutedIcon.style.visibility = "visible";
        audioPlaying = true;
        //audio.onended = audioEnded();
    }
    else {
        audio.pause();
        audio.currentTime = 0;
        mutedIcon.style.visibility = "hidden";
        audioPlaying = false;
    }
}

function audioEnded(){
    var mutedIcon = document.getElementById("muted");
    mutedIcon.style.visibility = "hidden";
    audioPlaying = false;
    audio.pause();
    audio.currentTime = 0;

}


function playVideo(){
    audioEnded();
    var videoScreen = document.getElementById("video-screen");
    var video = document.getElementById("videoId");
    videoScreen.style.visibility = "visible";
    video.play();
}

function stopVideo(){
    var videoScreen = document.getElementById("video-screen");
    var video = document.getElementById("videoId");
    video.pause();
    video.currentTime = 0;
    videoScreen.style.visibility = "hidden";
}


// global variables //
var audioPlaying = false;

